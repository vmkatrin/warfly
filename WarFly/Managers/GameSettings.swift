//
//  GameSettings.swift
//  WarFly
//
//  Created by Katrin on 23.03.2021.
//

import UIKit

class GameSettings: NSObject {
    
    let ud = UserDefaults.standard
    
    var isMusic = true
    var isSound = true
    
    let musicKey = "music"
    let soundKey = "sound"
    
    
    var highScore: [Int] = []
    var currenScore = 0
    let highScoreKey = "highScore"
    var isPlaying = false
    
    override init() {
        super.init()
        
        loadGameSettins()
        loadScores()
    }
    
    func saveScores() {
        print("save score \(currenScore)")
        highScore.append(currenScore)
        highScore = Array(highScore.sorted { $0 > $1 }.prefix(3))
        
        ud.set(highScore, forKey: highScoreKey)
        ud.synchronize()
    }
    
    func loadScores() {
        guard ud.value(forKey: highScoreKey) != nil else {return}
        highScore = ud.array(forKey: highScoreKey) as! [Int]
    }
    
    func saveGameSettings() {
        ud.set(isMusic, forKey: "music")
        ud.set(isSound, forKey: "sound")
    }
    
    func loadGameSettins() {
        guard ud.value(forKey: musicKey) != nil && ud.value(forKey: soundKey) != nil else { return }
        isMusic = ud.bool(forKey: musicKey)
        isSound = ud.bool(forKey: soundKey)
    }

}
